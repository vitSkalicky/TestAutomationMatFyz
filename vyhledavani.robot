*** Settings ***
Library  Browser

*** Variables ***

${sis_url}  https://is.cuni.cz/studium/
${name_search_term}  Úvod do softwarového

# expecteds
${exp_home_nazev_modulu}  Studijní informační systém
${exp_predmety_nazev_modulu}  Předměty
${exp_nazev_predmetu}  Úvod do softwarového inženýrství

# selectors
${loc_nazev_modulu}  id=stev_nazev_modulu
${loc_predmety}  id=hint_predmety
${loc_nazev_textfield}  id=nazev
${loc_result_name}  selector=.row1 > td:nth-child(4) > a:nth-child(1)  #ugly

*** Test Cases ***
Vyhledávání předmětu
    Open SIS
    Go to Předměty
    Enter search parameters
    Click  selector=.but_find  #performs search
    Check search results
    Sleep  10s
    #...

*** Keywords ***
Open SIS
    New Browser  browser=firefox  headless=False
    New Context  viewport={"width": 1000, "height": 600}
    New Page  url=${sis_url}
    Get Text  ${loc_nazev_modulu}  ==  ${exp_home_nazev_modulu}

Go to Předměty
    Click  ${loc_predmety}
    Get Text  ${loc_nazev_modulu}  ==  ${exp_predmety_nazev_modulu}

Enter search parameters
    Type Text  ${loc_nazev_textfield}  txt=${name_search_term}
    Check Checkbox  selector=#srch_pam_a
    Select Options By  select[id=sem]  text  letní
    Type Text  selector=#ujmeno  txt=Nečaský
    Click  selector=#utyp_2
   
Check search results
    Get Text  ${loc_result_name}  ==  ${exp_nazev_predmetu}